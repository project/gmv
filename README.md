# Google Model Viewer

gmv(Google Model Viewer) is a web component that makes rendering interactive
3D models.
gmv provides field type for drupal users to upload their 3D files(.zip).The 3D
file format should be glTF. file like https://sketchfab.com/features/gltf.

Here is the gmv field for 3D Objects...!!! You can create 3D files and upload
the .zip file to the Drupal gmv field in your content.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/gmv).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/gmv).

## Table of contents

- Requirements
- Installation
- Configuration
- Usage
- Creator and Maintainer

## Requirements

This module requires the following modules:

- [Core File Module] https://www.drupal.org/docs/8/core/modules/file

## Installation

Install as you would normally install a contributed Drupal module.

## Configuration

Create a gmv field type:
	Under your content type, create a new field of type "gmv"(3D).

Create content:
	Create a new content item and add the 3D .zip file.

Set properties for the 3D model as per the https://modelviewer.dev/:

	- Camera Controls
	- Touch-Action CSS property
	- Shadow Intensity - Shadow intensity for 3D object
	- Height in pixel - Height of the 3D object.
	- Width in pixel - Width of the 3D object. 

## Usage
Add your 3D file properties after uploading the .zip file to the Drupal field.

## Creator and Maintainer

Current maintainer:

- [Ajay T](https://www.drupal.org/u/ajay-t)
- [Hari Venu](https://www.drupal.org/u/harivenuv)

Supporting organizations:

- [Applab Qatar](https://www.drupal.org/applab-qatar)
