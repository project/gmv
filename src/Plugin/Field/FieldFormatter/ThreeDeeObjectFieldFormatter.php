<?php

namespace Drupal\gmv\Plugin\Field\FieldFormatter;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\File\FileUrlGenerator;

/**
 * Plugin implementation of the 'field_three_dee_object' formatter.
 *
 * @FieldFormatter(
 *   id = "field_three_dee_object",
 *   label = @Translation("three dee object formatter"),
 *   field_types = {
 *     "field_three_dee_object"
 *   }
 * )
 */
class ThreeDeeObjectFieldFormatter extends FormatterBase implements ContainerFactoryPluginInterface {
  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a ThreeDeeObjectFieldFormatter.
   *
   * @param string $plugin_id
   *   The plugin ID.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The field definition.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third-party settings.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */

  /**
   * The file URL generator.
   *
   * @var \Drupal\Core\File\FileUrlGeneratorInterface
   */
  protected FileUrlGenerator $fileUrlGenerator;

  public function __construct(
    string $plugin_id,
    $plugin_definition,
    FieldDefinitionInterface $field_definition,
    array $settings,
    string $label,
    string $view_mode,
    array $third_party_settings,
    EntityTypeManagerInterface $entity_type_manager,
    FileUrlGenerator $fileUrlGenerator,
  ) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
    $this->entityTypeManager = $entity_type_manager;
    $this->fileUrlGenerator = $fileUrlGenerator;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $field_definition = $configuration['field_definition'];

    return new static(
      $plugin_id,
      $plugin_definition,
      $field_definition,
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('entity_type.manager'),
      $container->get('file_url_generator')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    return [
    // Implement settings form.
    ] + parent::settingsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    // Implement settings summary.
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {

    $fileStorage = $this->entityTypeManager->getStorage('file');

    $elements = [];
    foreach ($items as $delta => $item) {

      $background_img_path = NULL;
      $path = NULL;

      if ($item->skybox_image_id != NULL) {

        $background_img_file = $fileStorage->load($item->skybox_image_id);
        if ($background_img_file->status->value != 1) {
          $background_img_file->setPermanent();
          $background_img_file->save();
        }
        $background_img_path = $this->fileUrlGenerator->generateAbsoluteString($background_img_file->getFileUri());

      }

      if ($item->three_dee_fid) {
        $file = $fileStorage->load($item->three_dee_fid);
        $path = $this->fileUrlGenerator->generateAbsoluteString($file->getFileUri());
      }

      $elements[$delta] = [
        '#theme' => 'gmv_image_formatter',
        '#uri' => $path,
        '#camera' => $item->camera,
        '#touch_action' => $item->touch_action,
        '#shadow' => $item->shadow,
        '#skybox_image' => $background_img_path,
      ];
    }

    return $elements;
  }

}
